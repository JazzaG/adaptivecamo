package au.com.gharib.jared.adaptivecamo;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;

class CamoSuit {

    protected static final Set<CamoSuit> SUITS = new HashSet<CamoSuit>();

    private int head;
    private int chest;
    private int legs;
    private int boots;

    public CamoSuit(int head, int chest,int legs,int boot) {
        this.head = head;
        this.chest = chest;
        this.legs = legs;
        this.boots = boot;
    }

    /**
     * Checks Player is wearing CamoSuit
     *
     * @param player Player to check
     * @return True if Player is wearing this suit
     */
    public boolean isWearing(Player player) {
        ItemStack[] armor = player.getInventory().getArmorContents();

        if(head != -1 && armor[3].getTypeId() != head)
            return false;
        if(chest != -1 && armor[2].getTypeId() != chest)
            return false;
        if(legs != -1 && armor[1].getTypeId() != legs)
            return false;
        if(boots != -1 && armor[0].getTypeId() != boots)
            return false;

        return true;
    }

    /**
     * Translates config suit entry into block/item ID
     *
     * @param suit Suit config value
     * @return Block/Item ID of entry, -1 if invalid.
     */
    public static int toSuitId(String suit) {
        int id;
        try {
            try {
                id = Integer.parseInt(suit);
            } catch (NumberFormatException ex) {
                id = Material.getMaterial(suit.toUpperCase()).getId();
            }
        } catch (IllegalArgumentException ex) {
            id = -1;
        }

        return id;
    }

}
