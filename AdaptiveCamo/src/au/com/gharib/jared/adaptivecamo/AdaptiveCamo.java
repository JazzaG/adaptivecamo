package au.com.gharib.jared.adaptivecamo;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;
import java.net.URL;
import java.util.logging.Level;

public class AdaptiveCamo extends JavaPlugin {

    private static AdaptiveCamo instance;

    private String latestVersion;
    private String latestDate;
    private String latestUrl;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        instance = this;

        // Register events
        getServer().getPluginManager().registerEvents(new FeatureListener(), this);
        getServer().getPluginManager().registerEvents(new BackendListener(), this);

        // Register command
        getCommand("adaptivecamo").setExecutor(new CommandBase());

        // Create PlayerInfo for online players
        CamoPlayer camoPlayer;
        for(Player p : getServer().getOnlinePlayers()) {
            camoPlayer = CamoPlayer.forPlayer(p);
            for(CamoState camoState : CamoState.values())
                camoPlayer.setState(camoState, false);
        }

        // Sort suits
        int suitNumber = 0;
        while(true) {
            if(suitNumber == 1)
                suitNumber = 2;

            String path = "suit" + (suitNumber == 0 ? "" : suitNumber);
            String head = getConfig().getString(path + ".head");
            if(head == null)
                break;

            CamoSuit cs = new CamoSuit(
                    CamoSuit.toSuitId(head),
                    CamoSuit.toSuitId(getConfig().getString(path + ".chest")),
                    CamoSuit.toSuitId(getConfig().getString(path + ".legs")),
                    CamoSuit.toSuitId(getConfig().getString(path + ".boots")));
            CamoSuit.SUITS.add(cs);

            suitNumber++;
        }

        // Version check
        log(Level.INFO, "Performing version check...");
        versionCheck();
        log(Level.INFO, "Done!");
        if(hasNewVersion()) {
            getLogger().log(Level.INFO, "Version " + getLatestVersion() + " found, released " + getLatestDate());
        }
    }

    @Override
    public void onDisable() {
        CamoPlayer.INSTANCES.clear();
    }


    /**
     * Gets the plugin instance
     *
     * @return AdaptiveCamo instance
     */
    protected static AdaptiveCamo getInstance() {
        return instance;
    }

    /**
     * Log a message via the plugin
     * <p />
     * Will check if server administrator has allowed debug messages to be sent
     *
     * @param level Log level to log
     * @param message Message to log
     */
    protected void log(Level level, String message) {
        if(getConfig().getBoolean("enable-debug-messages"))
            getLogger().log(level, message);
    }

    /**
     * Performs a version check
     *
     * @return success
     */
    protected boolean versionCheck() {
        if(!getConfig().getBoolean("enable-version-checking"))
            return false;

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader reader   = factory.createXMLEventReader(
                    new URL("http://dev.bukkit.org/server-mods/adaptivecamo/files.rss").openStream());

            while(reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                if(event.isStartElement()) {
                    if(event.asStartElement().getName().getLocalPart().equals("title")) {
                        event = reader.nextEvent();
                        latestVersion = event.asCharacters().getData().replaceAll("[^0-9\\.]", "");
                    } else if(event.asStartElement().getName().getLocalPart().equals("pubDate")) {
                        event = reader.nextEvent();
                        latestDate = event.asCharacters().getData().substring(5, 16);
                    } else if(event.asStartElement().getName().getLocalPart().equals("link")) {
                        event = reader.nextEvent();
                        latestUrl = event.asCharacters().getData();
                    }
                } else if(event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart().equals("item")) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log(Level.WARNING, "Couldn't perform a version check. Is your server connected to the internet?");
            return false;
        }

        return true;
    }

    /**
     * Checks if installed version is older than latest release
     * <p />
     * Does not check with config if version checking is allowed
     *
     * @return True if installed version is older than latest release
     */
    protected boolean hasNewVersion() {
        if(latestVersion == null)
            return false;

        try {
            int current = Integer.parseInt(getDescription().getVersion().replace(".", ""));
            return (current < Integer.parseInt(latestVersion.replace(".", "")));
        } catch (Exception ignored) {
        }

        return false;
    }

    /**
     * Gets version number of latest release from BukkitDEV
     *
     * @return Latest version number as a String exactly from BukkitDEV
     */
    protected String getLatestVersion() {
        return latestVersion;
    }

    /**
     * Gets date of latest release from BukkitDEV
     *
     * @return Date of latest release
     */
    protected String getLatestDate() {
        return latestDate;
    }

    /**
     * Gets URL permalink to latest release file on BukkitDEV
     *
     * @return URL of latest release
     */
    protected String getLatestUrl() {
        return latestUrl;
    }

}
