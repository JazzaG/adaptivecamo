package au.com.gharib.jared.adaptivecamo;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.*;

public class FeatureListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        // Get PlayerInfo
        Player player = e.getPlayer();
        CamoPlayer info = CamoPlayer.forPlayer(player);

        // Check valid player
        if(info == null)
            return;

        // Has requirements
        if(info.hasRequirements()) {
            info.activate();
        } else {
            info.deactivate();

            // Check early cooldown
            if(info.startsCooldownEarly())
                info.startCooldown();
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        // Get PlayerInfo
        Player player = e.getPlayer();
        CamoPlayer info = CamoPlayer.forPlayer(player);

        // Check valid player
        if(info == null)
            return;

        // Invisible only
        if(!info.isState(CamoState.INVISIBLITY))
            return;

        // Clicked block
        if(e.getClickedBlock() == null)
            return;

        if(!info.canInteract())
            e.setCancelled(true);
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent e) {
        // Player attacked
        if(!(e.getDamager() instanceof Player))
            return;

        // Get PlayerInfo
        Player player = (Player) e.getDamager();
        CamoPlayer info = CamoPlayer.forPlayer(player);

        // Check valid player
        if(info == null)
            return;

        // Invisible only
        if(!info.isState(CamoState.INVISIBLITY))
            return;

        // What was attacked
        if(e.getEntity() instanceof Player) {
            if(!info.canAttackPlayers())
                e.setCancelled(true);
        } else {
            if(!info.canAttackMobs())
                e.setCancelled(true);
        }

        // Early cooldown
        if(!e.isCancelled()) {
            if(info.startsCooldownOnAttack())
                info.startCooldown();
        }

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        // Get PlayerInfo
        Player player = e.getPlayer();
        CamoPlayer info = CamoPlayer.forPlayer(player);

        // Check valid player
        if(info == null)
            return;

        // Invisible
        if(!info.isState(CamoState.INVISIBLITY))
            return;

        if(!info.canBreakBlocks())
            e.setCancelled(true);
    }

    @EventHandler
    public void onEntityTarget(EntityTargetEvent e) {
        // Target is player
        if(!(e.getTarget() instanceof Player))
            return;

        // Get PlayerInfo
        Player player = (Player) e.getTarget();
        CamoPlayer info = CamoPlayer.forPlayer(player);

        // Check valid player
        if(info == null)
            return;

        // Invisible
        if(!info.isState(CamoState.INVISIBLITY))
            return;

        if(info.canUseMobDisguise()) {
            e.setCancelled(true);
            e.setTarget(null);
        }
    }





}
