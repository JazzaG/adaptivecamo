package au.com.gharib.jared.adaptivecamo;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBase implements CommandExecutor {

    private AdaptiveCamo plugin;

    public CommandBase() {
        plugin = AdaptiveCamo.getInstance();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        try {
            if(sender instanceof Player && !sender.hasPermission("adaptivecamo.admin")) {
                sender.sendMessage(ChatColor.RED + "You don't have permission to use this command!");
                return true;
            }

            if("reload".equalsIgnoreCase(args[0])) {
                return reload(sender);
            } else if("check".equalsIgnoreCase(args[0])) {
                return check(sender);
            } else
                throw new ArrayIndexOutOfBoundsException();
        } catch (ArrayIndexOutOfBoundsException ex) {
            String[] msgs = new String[] {
                    "/adaptivecamo reload: Reload the config",
                    "/adaptivecamo check: Check for new release",
            };

            for(String m : msgs)
                sender.sendMessage(m);
            return true;
        }
    }

    private boolean reload(CommandSender sender) {
        plugin.onDisable();
        plugin.reloadConfig();
        plugin.onEnable();
        sender.sendMessage(ChatColor.GREEN + "AdaptiveCamo successfully reloaded");

        return true;
    }

    private boolean check(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "Contacting BukkitDev servers...");
        if(plugin.versionCheck()) {
            if(plugin.hasNewVersion()) {
                sender.sendMessage(ChatColor.DARK_PURPLE + "New version found: " + plugin.getLatestVersion());
                sender.sendMessage(ChatColor.DARK_PURPLE + "Released on " + plugin.getLatestDate());
                sender.sendMessage(ChatColor.DARK_PURPLE + "http://dev.bukkit.org/server-mods/adaptivecamo");
            } else
                sender.sendMessage(ChatColor.GREEN + "AdaptiveCamo is up to date "
                        + "(" + plugin.getDescription().getVersion() + ")");
        } else {
            sender.sendMessage(ChatColor.RED + "Failed! Check config and or internet connection.");
        }

        return true;
    }

}
