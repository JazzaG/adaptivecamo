package au.com.gharib.jared.adaptivecamo;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class BackendListener implements Listener {

    private boolean shown = false;

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        if(player.hasPermission("adaptivecamo.admin") && !shown) {
            AdaptiveCamo ac = AdaptiveCamo.getInstance();
            if(ac.hasNewVersion() && ac.getLatestVersion() != null) {
                player.sendMessage(ChatColor.DARK_PURPLE + "[AdaptiveCamo] Version " + ac.getLatestVersion()
                        + "ready for download");
                player.sendMessage("\n" + ChatColor.DARK_PURPLE + ac.getLatestUrl());

                shown = true;
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        CamoPlayer.INSTANCES.remove(e.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerKickEvent e) {
        CamoPlayer.INSTANCES.remove(e.getPlayer());
    }

}
