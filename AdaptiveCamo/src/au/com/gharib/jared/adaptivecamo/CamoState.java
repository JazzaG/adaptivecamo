package au.com.gharib.jared.adaptivecamo;

public enum CamoState {

    /** State of invisibility */
    INVISIBLITY,

    /** State of running usage timer */
    USAGE,

    /** State of running cooldown timer */
    COOLDOWN;

}

