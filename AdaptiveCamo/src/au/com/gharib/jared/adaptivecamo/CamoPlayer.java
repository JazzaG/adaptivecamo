package au.com.gharib.jared.adaptivecamo;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.*;

class CamoPlayer {

    private static final AdaptiveCamo PLUGIN = AdaptiveCamo.getInstance();
    protected static final Map<Player, CamoPlayer> INSTANCES = new HashMap<Player, CamoPlayer>();

    private Player player;
    private Set<CamoState> states;
    private Long usageTimer    = 0L;
    private Long cooldownTimer = 0L;


    /**
     * Gets or creates an instance for player
     *
     * @param player Player
     * @return Instance
     */
    public static CamoPlayer forPlayer(Player player) {
        CamoPlayer instance = INSTANCES.get(player);
        return (instance == null) ? new CamoPlayer(player) : instance;
    }

    /**
     * Creates instance and sets usage and cooldown timer
     *
     * @param p Player to instantiate for
     */
    private CamoPlayer(Player p) {
        this.player = p;
        this.states = new HashSet<CamoState>();

        // Get usage and cooldown timers
        String group = "default";
        for(Object object : PLUGIN.getConfig().getConfigurationSection("timers").getKeys(false)) {
            if(player.hasPermission("adaptivecamo.timer." + object.toString())) {
                group = object.toString();
                break;
            }
        }
        this.usageTimer    = PLUGIN.getConfig().getLong(String.format("timers.%s.usage", group));
        this.cooldownTimer = PLUGIN.getConfig().getLong(String.format("timers.%s.cooldown", group));

        // Save
        INSTANCES.put(player, this);
    }


    /**
     * Sets the state to the boolean value
     *
     * @param state CamoState to set
     * @param bool Value to set to
     */
    public void setState(CamoState state, boolean bool) {
        if(bool)
            states.add(state);
        else
            states.remove(state);
    }

    /**
     * Checks if player is in CamoState
     *
     * @param state The state to check
     * @return True if player is in the state
     */
    public boolean isState(CamoState state) {
        return states.contains(state);
    }

    /**
     * Gets player's usage timer in seconds
     *
     * @return Player's usage timer
     */
    public Long getUsageTimer() {
        return usageTimer;
    }

    /**
     * Gets player's cooldown timer in seconds
     *
     * @return Player's cooldown timer
     */
    public Long getCooldownTimer() {
        return cooldownTimer;
    }

    /**
     * Gets whether player's suit starts cooldown as soon as it deactivates
     *
     * @return True if suit's cooldown starts on de-activation
     */
    public boolean startsCooldownEarly() {
        return player.hasPermission("adaptivecamo.active.earlycooldown");
    }

    /**
     * Gets whether player's suit starts cooldown when player attacks
     *
     * @return True if suit deactivates when player attacks
     */
    public boolean startsCooldownOnAttack() {
        return player.hasPermission("adaptivecamo.active.earlycooldown.attack");
    }

    /**
     * Gets whether player can attack mobs while invisible
     *
     * @return True if player can
     */
    public boolean canAttackMobs() {
        return player.hasPermission("adaptivecamo.active.entity");
    }

    /**
     * Gets whether player can attack other players while invisible
     *
     * @return True if player can
     */
    public boolean canAttackPlayers() {
        return player.hasPermission("adaptivecamo.active.player");
    }

    /**
     * Gets whether player can break blocks while invisible
     *
     * @return True if player can
     */
    public boolean canBreakBlocks() {
        return player.hasPermission("adaptivecamo.active.break");
    }

    /**
     * Gets whether player can interact with levers, buttons, doors etc while invisible
     *
     * @return True if interaction is allowed
     */
    public boolean canInteract() {
        return player.hasPermission("adaptivecamo.active.interact");
    }

    /**
     * Gets whether player is hidden from mobs while invisible
     *
     * @return True if player can
     */
    public boolean canUseMobDisguise() {
        return player.hasPermission("adaptivecamo.active.mobdisguise");
    }

    /**
     * Checks if player has requirements for activating suit
     *
     * @return True if player meets requirements
     */
    public boolean hasRequirements() {
        boolean wearing = false;
        for(CamoSuit cs : CamoSuit.SUITS) {
            if(cs.isWearing(player))
                wearing = true;
        }

        return (player.hasPermission("adaptivecamo.use") &&
                player.getItemInHand().getType() == Material.AIR &&
                player.isSneaking() &&
                !isState(CamoState.COOLDOWN) &&
                wearing);
    }

    /**
     * Starts the usage timer for the player
     */
    public void startUsage() {
        // Get length from config
        Long timer = getUsageTimer() * 20;

        // Check it isn't 0
        if(timer == 0) return;

        // Check timer isn't already going
        if(!isState(CamoState.USAGE)) {
            // Start cooldown after 'timer' seconds
            PLUGIN.getServer().getScheduler().scheduleSyncDelayedTask(PLUGIN, new Runnable() {
                public void run() {
                    startCooldown();
                }
            }, timer);
        }

        // Set usage to true
        setState(CamoState.USAGE, true);
    }

    /**
     * Starts the cooldown timer for the player
     */
    public void startCooldown() {
        // Get cooldown from config
        Long timer = getCooldownTimer() * 20;

        // Check it isn't 0
        if(timer == 0) return;

        // Set cooldown to true
        setState(CamoState.COOLDOWN, true);

        // Force deactivation
        deactivate();

        // Start timer
        PLUGIN.getServer().getScheduler().scheduleSyncDelayedTask(PLUGIN, new Runnable() {
            public void run() {
                // Turn cooldown off
                setState(CamoState.COOLDOWN, false);
                // Reset usage
                setState(CamoState.USAGE, false);
            }
        }, timer);

    }

    /**
     * Activates a player's suit
     */
    public void activate() {
        // Start usage timer
        if(!isState(CamoState.USAGE))
            startUsage();

        // Only if visible
        if(isState(CamoState.INVISIBLITY))
            return;

        // Check another plugin hasn't hidden player
        if(isVanished())
            return;

        // Hide player
        for(Player p : PLUGIN.getServer().getOnlinePlayers()) {
            if(!p.hasPermission("adaptivecamo.see"))
                p.hidePlayer(player);
        }

        // Show activation message
        if(PLUGIN.getConfig().getBoolean("activation-message.show")) {
            String colour  = PLUGIN.getConfig().getString("activation-message.colour");
            String message = PLUGIN.getConfig().getString("activation-message.message");

            player.sendMessage(ChatColor.valueOf(colour.toUpperCase()) + message);
        }

        // Activate mob disguise
        if(canUseMobDisguise()) {
            Creature creature;
            for(final Entity entity : player.getNearbyEntities(100D, 100D, 100D)) {
                if(!(entity instanceof Creature))
                    continue;
                creature = (Creature) entity;

                if(creature.getTarget() != null && creature.getTarget().equals(player))
                    creature.setTarget(null);
            }
        }

        // Set state
        setState(CamoState.INVISIBLITY, true);
    }

    /**
     * Deactivates a player's suit
     */
    public void deactivate() {
        // Only if hidden
        if(!isState(CamoState.INVISIBLITY))
            return;

        // Show player
        for(Player p : PLUGIN.getServer().getOnlinePlayers())
            p.showPlayer(player);

        // Show deactivation message
        if(PLUGIN.getConfig().getBoolean("deactivation-message.show")) {
            String colour  = PLUGIN.getConfig().getString("deactivation-message.colour");
            String message = PLUGIN.getConfig().getString("deactivation-message.message");
            player.sendMessage(ChatColor.valueOf(colour.toUpperCase()) + message);
        }

        setState(CamoState.INVISIBLITY, false);
    }

    /**
     * Checks if player is hidden to anyone on server
     *
     * @return True if any player cannot see Player
     */
    public boolean isVanished() {
        for(Player p : PLUGIN.getServer().getOnlinePlayers()) {
            if(!p.canSee(player))
                return true;
        }

        return false;
    }
    
}
